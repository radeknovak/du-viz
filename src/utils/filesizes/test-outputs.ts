export const output1 = `
32	/Users/rdkn/.config
128	/Users/rdkn/Music
7384	/Users/rdkn/.node-gyp
1794576	/Users/rdkn/gh
0	/Users/rdkn/.thumbnails
524296	/Users/rdkn/bin
8	/Users/rdkn/Pictures
159480	/Users/rdkn/.nvm
503616	/Users/rdkn/code
19464	/Users/rdkn/Desktop
21285456	/Users/rdkn/Library
79480	/Users/rdkn/Calibre Library
16	/Users/rdkn/Public
120360	/Users/rdkn/.dropbox
13938104	/Users/rdkn/MEGA
104	/Users/rdkn/.ssh
0	/Users/rdkn/Movies
4416	/Users/rdkn/Applications
0	/Users/rdkn/MEGAsync
2680	/Users/rdkn/Dropbox
25758016	/Users/rdkn/.Trash
616	/Users/rdkn/.zoomus
212744	/Users/rdkn/.npm
1384	/Users/rdkn/Documents
488888	/Users/rdkn/.vscode
39472	/Users/rdkn/.oh-my-zsh
33107976	/Users/rdkn/Downloads
16	/Users/rdkn/.cache
1416	/Users/rdkn/.zsh
98050472	/Users/rdkn

`;

export const output2 = `

40	src/utils/filesizes
64	src/utils
40	src/components
8	src/containers
20808	src

`;

export const output2h = `
32K    src/utils/filesizes
44K    src/utils
20K    src/components
4.0K    src/containers
10M    src
`;

export const output3 = `
40	./src/utils/filesizes
64	./src/utils
40	./src/components
8	./src/containers
20808	./src
`;

export const output4 = `
8	../../../../System/DriverKit/System/Library/Frameworks/HIDDriverKit framework/_CodeSignature
368	../../../../System/DriverKit/System/Library/Frameworks/HIDDriverKit framework
8	../../../../System/DriverKit/System/Library/Frameworks/DriverKit framework/_CodeSignature
280	../../../../System/DriverKit/System/Library/Frameworks/DriverKit framework
8	../../../../System/DriverKit/System/Library/Frameworks/SerialDriverKit framework/_CodeSignature
56	../../../../System/DriverKit/System/Library/Frameworks/SerialDriverKit framework
8	../../../../System/DriverKit/System/Library/Frameworks/NetworkingDriverKit framework/_CodeSignature
136	../../../../System/DriverKit/System/Library/Frameworks/NetworkingDriverKit framework
8	../../../../System/DriverKit/System/Library/Frameworks/USBSerialDriverKit framework/_CodeSignature
48	../../../../System/DriverKit/System/Library/Frameworks/USBSerialDriverKit framework
8	../../../../System/DriverKit/System/Library/Frameworks/USBDriverKit framework/_CodeSignature
80	../../../../System/DriverKit/System/Library/Frameworks/USBDriverKit framework
968	../../../../System/DriverKit/System/Library/Frameworks
968	../../../../System/DriverKit/System/Library
968	../../../../System/DriverKit/System
`;
export const output5 = `
 4.0K	../../../../System/DriverKit/System/Library/Frameworks/HIDDriverKit.framework/_CodeSignature
184K	../../../../System/DriverKit/System/Library/Frameworks/HIDDriverKit.framework
4.0K	../../../../System/DriverKit/System/Library/Frameworks/DriverKit.framework/_CodeSignature
140K	../../../../System/DriverKit/System/Library/Frameworks/DriverKit.framework
4.0K	../../../../System/DriverKit/System/Library/Frameworks/SerialDriverKit.framework/_CodeSignature
 28K	../../../../System/DriverKit/System/Library/Frameworks/SerialDriverKit.framework
4.0K	../../../../System/DriverKit/System/Library/Frameworks/NetworkingDriverKit.framework/_CodeSignature
 68K	../../../../System/DriverKit/System/Library/Frameworks/NetworkingDriverKit.framework
4.0K	../../../../System/DriverKit/System/Library/Frameworks/USBSerialDriverKit.framework/_CodeSignature
 24K	../../../../System/DriverKit/System/Library/Frameworks/USBSerialDriverKit.framework
4.0K	../../../../System/DriverKit/System/Library/Frameworks/USBDriverKit.framework/_CodeSignature
 40K	../../../../System/DriverKit/System/Library/Frameworks/USBDriverKit.framework
484G	../../../../System/DriverKit/System/Library/Frameworks
484G	../../../../System/DriverKit/System/Library
484G	../../../../System/DriverKit/System

 `;
