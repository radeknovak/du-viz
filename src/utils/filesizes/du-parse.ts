type Tree = {
  [path: string]: {
    size: number;
    children: Tree;
  };
};

const unitMap = {
  B: 1, // Byte
  K: 1e3, // Kilobyte
  M: 1e6, // Megabyte
  G: 1e9, // Gigabyte
  T: 1e12, // Terabyte
  P: 1e15 // Petabyte
} as const;

const reSize = /(?<amount>[0-9.]+)(?<unit>[BKMGTP])/;
const rePathSplit = /(.*?)\s(.*)/;

export const getDUlines = (duStr: string) =>
  duStr
    .split('\n')
    .map(line => line.trim())
    .filter(line => Boolean(line.length));

export const getPathSizeSplit = (
  strLines: string[],
  options?: Partial<{ formatSize: (s: string) => string }>
) => {
  const formatSize = options?.formatSize ?? (s => s);
  return strLines.map(line => {
    const [, size, path] = line.match(rePathSplit) ?? [0, '', ''];

    return [(path as string).toString().trim(), formatSize(size.toString())];
  });
};

const arrayShallowEquals = (a: any[], b: any[]) => {
  if (a.length !== b.length) return false;
  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) return false;
  }
  return true;
};

/**
 * Operates on structures as: [['file', 'path'], '3B'][]
 *
 * @param paths
 */
export const longestCommonPrefix = (paths: (readonly [string[], string])[]) => {
  // Start with the first one as current shortest
  let currentShortest = paths[0][0];

  // for (let [path] of paths) {
  for (let i = 1; i < paths.length; i++) {
    const [path] = paths[i];

    // trim to same size
    const len = Math.min(path.length, currentShortest.length);
    let temp = [];

    for (let i = 0; i < len; i++) {
      if (currentShortest[i] === path[i]) temp.push(currentShortest[i]);
      else break;
    }

    currentShortest = temp;

    // No common prefix
    if (currentShortest.length === 0) break;
  }

  return currentShortest;
};

/**
 * Parses a value coming from `du -h`: 12.3K, 150M, etc.
 */
export const parseSize = (value: string) => {
  const match = value.match(reSize);

  if (match?.groups) {
    const { amount, unit } = match.groups;

    // no idea why I have to cast the `unit`
    const multiplier =
      unit in unitMap ? unitMap[unit as keyof typeof unitMap] : 1;

    return parseFloat(amount) * multiplier;
  }

  return parseInt(value, 10);
};

/**
 * Builds tree structure from the `du` command output
 *
 * @param duStr raw input from the `du` command
 * @param options
 */
export const buildTree = (
  duStr: string,
  options?: {
    pathParser?: (s: string) => string[];
    sizeParser?: (s: string) => number;
  }
) => {
  const formatSize = options?.sizeParser ?? parseSize;
  const pathParser = options?.pathParser ?? (s => s.split('/'));

  const lines = getDUlines(duStr);
  const splitLines = getPathSizeSplit(lines);
  const parsedLines = splitLines.map(
    ([path, size]) => [pathParser(path), size] as const
  );
  const commonPrefix = longestCommonPrefix(parsedLines);

  const trimmedLines = splitLines.map(
    ([path, size]) =>
      [pathParser(path).slice(commonPrefix.length), size] as const
  );
  const root = parsedLines.find(([pathFragments]) =>
    arrayShallowEquals(pathFragments, commonPrefix)
  );

  const ret: Tree = {};
  let parent = ret;

  // If a common prefix was found, we start from there
  if (commonPrefix.length) {
    const prefixString = commonPrefix.join('/');
    ret[prefixString] = {
      // If the root is found in the paths we use its size
      size: root ? formatSize(root[1]) : 0,
      children: {}
    };

    parent = ret[prefixString].children;
  }

  for (let [path, size] of trimmedLines) {
    let ref = parent;

    for (let pathFragment of path) {
      if (!(pathFragment in ref)) {
        ref[pathFragment] = { size: 0, children: {} };
      }

      const isLast = pathFragment === path[path.length - 1];
      if (isLast) {
        ref[pathFragment].size = formatSize(size);
      } else {
        ref = ref[pathFragment].children;
      }
    }
  }

  return ret;
};

/**
 * {
 *  a: {
 *   b: 1,
 *   c: 2,
 *   d: {
 *     dd: 22
 *   }
 *  }
 * }
 * ['a, 'd']
 * => { d: { dd: 22 } }
 *
 */
export const lensView = (obj: Tree, lensPath: string[]) => {
  // a simple (and probably slow) deep clone
  let current: Tree = JSON.parse(JSON.stringify(obj));

  const lastKey = lensPath[lensPath.length - 1];
  const keyPath = lensPath.slice(0, -1);
  for (let key of keyPath) {
    const _current = current[key];
    if (_current) current = _current.children;
    else {
      current = {};
      break;
    }
  }

  for (let key of Object.keys(current)) {
    if (key !== lastKey) delete current[key];
  }

  return current;
};
