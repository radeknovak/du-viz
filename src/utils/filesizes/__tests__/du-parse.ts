import {
  getDUlines,
  getPathSizeSplit,
  parseSize,
  longestCommonPrefix
} from '../du-parse';
import { output2, output4 } from '../test-outputs';

const test1 = `16K	node_modules/arrify
4.0K	node_modules/postcss-reduce-initial/dist
 92K	node_modules/postcss-reduce-initial/node_modules/browserslist
  0B	node_modules/postcss-reduce-initial/node_modules/.bin
216K	node_modules/postcss-reduce-initial/node_modules/node-releases
3.1M	node_modules/postcss-reduce-initial/node_modules/caniuse-lite
 52K	node_modules/postcss-reduce-initial/node_modules/electron-to-chromium
3.4M	node_modules/postcss-reduce-initial/node_modules
 12K	node_modules/postcss-reduce-initial/data
3.5M	node_modules/postcss-reduce-initial
4.0K	node_modules/setprototypeof/test
 24K	node_modules/setprototypeof
410M	node_modules`;

test('get clean lines', () => {
  const outlines = getDUlines(output2);
  expect(outlines).toEqual([
    '40	src/utils/filesizes',
    '64	src/utils',
    '40	src/components',
    '8	src/containers',
    '20808	src'
  ]);
});

test('get clean lines and trim', () => {
  expect(getDUlines(test1)).toEqual([
    '16K	node_modules/arrify',
    '4.0K	node_modules/postcss-reduce-initial/dist',
    '92K	node_modules/postcss-reduce-initial/node_modules/browserslist',
    '0B	node_modules/postcss-reduce-initial/node_modules/.bin',
    '216K	node_modules/postcss-reduce-initial/node_modules/node-releases',
    '3.1M	node_modules/postcss-reduce-initial/node_modules/caniuse-lite',
    '52K	node_modules/postcss-reduce-initial/node_modules/electron-to-chromium',
    '3.4M	node_modules/postcss-reduce-initial/node_modules',
    '12K	node_modules/postcss-reduce-initial/data',
    '3.5M	node_modules/postcss-reduce-initial',
    '4.0K	node_modules/setprototypeof/test',
    '24K	node_modules/setprototypeof',
    '410M	node_modules'
  ]);
});

test('get split lines', () => {
  const outlines = getPathSizeSplit(getDUlines(output2));
  expect(outlines).toEqual([
    ['src/utils/filesizes', '40'],
    ['src/utils', '64'],
    ['src/components', '40'],
    ['src/containers', '8'],
    ['src', '20808']
  ]);
});

test('get split lines', () => {
  const testlines = getPathSizeSplit(getDUlines(test1));
  expect(testlines).toEqual([
    ['node_modules/arrify', '16K'],
    ['node_modules/postcss-reduce-initial/dist', '4.0K'],
    ['node_modules/postcss-reduce-initial/node_modules/browserslist', '92K'],
    ['node_modules/postcss-reduce-initial/node_modules/.bin', '0B'],
    ['node_modules/postcss-reduce-initial/node_modules/node-releases', '216K'],
    ['node_modules/postcss-reduce-initial/node_modules/caniuse-lite', '3.1M'],
    [
      'node_modules/postcss-reduce-initial/node_modules/electron-to-chromium',
      '52K'
    ],
    ['node_modules/postcss-reduce-initial/node_modules', '3.4M'],
    ['node_modules/postcss-reduce-initial/data', '12K'],
    ['node_modules/postcss-reduce-initial', '3.5M'],
    ['node_modules/setprototypeof/test', '4.0K'],
    ['node_modules/setprototypeof', '24K'],
    ['node_modules', '410M']
  ]);
});

test('get split lines', () => {
  const outlines = getPathSizeSplit(getDUlines(output4));
  expect(outlines).toEqual([
    [
      '../../../../System/DriverKit/System/Library/Frameworks/HIDDriverKit framework/_CodeSignature',
      '8'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/HIDDriverKit framework',
      '368'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/DriverKit framework/_CodeSignature',
      '8'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/DriverKit framework',
      '280'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/SerialDriverKit framework/_CodeSignature',
      '8'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/SerialDriverKit framework',
      '56'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/NetworkingDriverKit framework/_CodeSignature',
      '8'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/NetworkingDriverKit framework',
      '136'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/USBSerialDriverKit framework/_CodeSignature',
      '8'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/USBSerialDriverKit framework',
      '48'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/USBDriverKit framework/_CodeSignature',
      '8'
    ],
    [
      '../../../../System/DriverKit/System/Library/Frameworks/USBDriverKit framework',
      '80'
    ],
    ['../../../../System/DriverKit/System/Library/Frameworks', '968'],
    ['../../../../System/DriverKit/System/Library', '968'],
    ['../../../../System/DriverKit/System', '968']
  ]);
});

test('convert units', () => {
  expect(parseSize('2.45M')).toBe(2_450_000);
  expect(parseSize('184K')).toBe(184_000);
  expect(parseSize('4.0K')).toBe(4_000);
  expect(parseSize('4.1K')).toBe(4_100);
  expect(parseSize('0B')).toBe(0);
  expect(parseSize('10B')).toBe(10);
  expect(parseSize('103')).toBe(103);
  expect(parseSize(' 4.1K')).toBe(4_100);
  expect(parseSize(' 103')).toBe(103);
});

test('longest common prefix', () => {
  expect(
    longestCommonPrefix([
      [['ab', 'cd'], '3B'],
      [['ab', 'ce'], '3B'],
      [['ab', 'cf'], '3B'],
      [['ab', 'ce', 'ee'], '3B']
    ])
  ).toEqual(['ab']);
  expect(
    longestCommonPrefix([
      [['bcd'], '4B'],
      [['abce'], '3K']
    ])
  ).toEqual([]);
});
