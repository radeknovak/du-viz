import { buildTree } from '../du-parse';
import nm from '../example-data/nm';
import nmOnedot from '../example-data/nm-onedot';
import nmTwodots from '../example-data/nm-twodots';
import nmFourdots from '../example-data/nm-fourdots';

test('large data', () => {
  const nmTree = buildTree(nm);

  expect(buildTree(nmOnedot)['.']).toEqual(nmTree.node_modules);
  expect(buildTree(nmTwodots)['..']).toEqual(nmTree.node_modules);
  expect(buildTree(nmFourdots)['../..']).toEqual(nmTree.node_modules);
});
