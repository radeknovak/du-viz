import { buildTree } from '../du-parse';
import { output2h, output2 } from '../test-outputs';

test('construct tree', () => {
  expect(buildTree(output2)).toEqual({
    src: {
      size: 20808,
      children: {
        containers: {
          size: 8,
          children: {}
        },
        components: {
          size: 40,
          children: {}
        },
        utils: {
          size: 64,
          children: {
            filesizes: {
              size: 40,
              children: {}
            }
          }
        }
      }
    }
  });
});

test('construct tree with human-readable sizes', () => {
  expect(buildTree(output2h)).toEqual({
    src: {
      size: 10_000_000,
      children: {
        containers: {
          size: 4000,
          children: {}
        },
        components: {
          size: 20_000,
          children: {}
        },
        utils: {
          size: 44_000,
          children: {
            filesizes: {
              size: 32_000,
              children: {}
            }
          }
        }
      }
    }
  });
});

test('construct tree with two roots', () => {
  const testcase = `
  24K	root-a/child
   33B  root-b/child
   44M  root-a
  410M	root-b`;

  expect(buildTree(testcase)).toEqual({
    'root-a': {
      size: 44_000_000,
      children: {
        child: {
          size: 24_000,
          children: {}
        }
      }
    },
    'root-b': {
      size: 410_000_000,
      children: {
        child: {
          size: 33,
          children: {}
        }
      }
    }
  });
});
test('common root', () => {
  const testcase = `
  3K	a/b/c/d/e/f
  1K	a/b/c/d/e/f/child_a
  1K	a/b/c/d/e/f/child_b
  1K	a/b/c/d/e/f/child_c
  `;

  expect(buildTree(testcase)).toEqual({
    'a/b/c/d/e/f': {
      size: 3000,
      children: {
        child_a: {
          size: 1_000,
          children: {}
        },
        child_b: {
          size: 1_000,
          children: {}
        },
        child_c: {
          size: 1_000,
          children: {}
        }
      }
    }
  });
});
test('common nonexistent root', () => {
  const testcase = `
  1K	a/b/c/d/e/f/child_a
  1K	a/b/c/d/e/f/child_b
  1K	a/b/c/d/e/f/child_c
  `;

  expect(buildTree(testcase)).toEqual({
    'a/b/c/d/e/f': {
      size: 0,
      children: {
        child_a: {
          size: 1_000,
          children: {}
        },
        child_b: {
          size: 1_000,
          children: {}
        },
        child_c: {
          size: 1_000,
          children: {}
        }
      }
    }
  });
});
