import React, { Component } from 'react';
import Viz from '../components/Viz';
import InputScreen from '../components/InputScreen';
import { buildTree, lensView } from '../utils/filesizes/du-parse';
import Breadcrumbs from '../components/Breadcrumbs';
import pathSize from '../components/PathSizeIndicator';

type Path = string[];
type Size = any;
type HandleEvent = {
  path: Path;
  size?: Size;
};

export default class HomePage extends Component {
  state = {
    currentPath: [],
    viewing: { path: null, size: 0 },
    currentTreeData: null,
    treeData: null,
    loading: true,
    textareaInput: null
  };

  load = (duOutput: string) => {
    const treeData = buildTree(duOutput);
    const currentPath = [Object.keys(treeData)[0]];

    this.setState({
      currentPath,
      treeData,
      currentTreeData: lensView(treeData, currentPath),
      loading: false
    });
  };

  handleHover = ({ size, path }: HandleEvent) => {
    this.setState({
      viewing: {
        size,
        path
      }
    });
  };

  handleClick = ({ path, size }: HandleEvent) => {
    const { treeData } = this.state;
    if (!treeData) return;

    const currentPath = [...this.state.currentPath.slice(0, -1), ...path];
    this.setState({
      currentPath,
      currentTreeData: lensView(treeData || {}, currentPath)
    });
  };

  breadClick = (path: Path) => {
    const { treeData } = this.state;
    if (!treeData) return;

    this.setState({
      currentPath: path,
      currentTreeData: lensView(treeData || {}, path)
    });
  };

  render() {
    const {
      viewing: { path, size },
      loading,
      currentTreeData,
      currentPath
    } = this.state;

    const combinedPath = currentPath.slice(0, -1).concat(path || []);

    return (
      <>
        {currentTreeData ? (
          <div className="layout">
            <div className="TopBar">
              {loading ? (
                'Loading...'
              ) : (
                <Breadcrumbs
                  combinedPath={combinedPath}
                  path={currentPath}
                  onClick={this.breadClick}
                />
              )}
            </div>
            <Viz
              data={currentTreeData}
              onHover={this.handleHover}
              onClick={this.handleClick}
            />
            <div className="BottomBar">
              {pathSize({ path: combinedPath, size })}
            </div>
          </div>
        ) : (
          <InputScreen onSubmit={this.load} />
        )}
      </>
    );
  }
}
